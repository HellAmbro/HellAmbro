## [Hi,<img src="https://raw.githubusercontent.com/ABSphreak/ABSphreak/master/gifs/Hi.gif" width="30px"> I'm Francesco][website]
[![LinkedIn](https://img.shields.io/badge/LinkedIn-Francesco%20Ambrosini-green)](https://www.linkedin.com/in/francesco-ambrosini-2493ab140/)
[![Instagram](https://img.shields.io/badge/Instagram-hell__ambro98-yellow)](https://www.instagram.com/hell_ambro98/)
[![Facebook](https://img.shields.io/badge/facebook-Francesco%20Ambrosini-blue)](https://www.facebook.com/fraambro98/)
[![Website](https://img.shields.io/badge/website-Francesco%20Ambrosini-red)][website]
#### 📫 Contact me: [email](mailto://frambrosini1998@gmail.com)
<a href="https://github.com/HellAmbro">
  <img height="175em" src="https://github-readme-stats.vercel.app/api?username=HellAmbro&theme=react&show_icons=true&count_private=true"/>
  <img height="175em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=HellAmbro&theme=react&layout=compact&langs_count=7"/>
</a>
<a href="https://github.com/HellAmbro"><img width="819em" src="https://activity-graph.herokuapp.com/graph?username=HellAmbro&bg_color=20232a&color=58a6ff&line=114a88&point=58a6ff&hide_border=true" /></a>

<div style="display: inline_block"><br>
  <img align="center" alt="Python" height="45" width="60" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <img align="center" alt="Java" height="45" width="60" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <img align="center" alt="Javascript" height="45" width="60" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/kotlin/kotlin-original.svg">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <img align="center" alt="Typescript" height="45" width="60" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <img align="center" alt="HTML" height="45" width="60" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/android/android-original.svg">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <img align="center" alt="CSS" height="45" width="60" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <img align="center" alt="Git" height="45" width="60" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/git/git-original.svg">
</div>

### Spoken languages
- Italian (native)
- English
- Spanish
### Programing Languages
- Java
- Kotlin
- C
- Python
### OS
- Android
- Windows
- Linux
### Frameworks
- Django
- Spring
- Bootstrap
- Flask
#
I have published 3 applications on the Play Store under the name [Rutershok](https://play.google.com/store/apps/dev?id=6921566180913144685&hl=en_CA)  
- [Phrases](https://play.google.com/store/apps/details?id=com.rutershok.phrases)
- [Quotes](https://play.google.com/store/apps/details?id=com.rutershok.quotes)
- [Daily Quotes](https://play.google.com/store/apps/details?id=com.rutershok.daily)

[website]: https://www.francescoambrosini.it/

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=hellambro&theme=dracula)](https://github.com/anuraghazra/github-readme-stats)

[![Catalin's GitHub stats](https://github-readme-stats.vercel.app/api?username=hellambro&theme=dracula)](https://github.com/anuraghazra/github-readme-stats)

